//
//  VespaObject.swift
//  GestureRecognition
//
//  Created by Alessio Spina on 28/03/2019.
//  Copyright © 2019 Alessio Spina. All rights reserved.
//

import Foundation
import SceneKit

class VespaObject: SceneObject {
    
    var animating: Bool = false
    
    //Variables to manage movements
    var movingForward: Bool = false
    var movingBack: Bool = false
    var movingLeft: Bool = false
    var movingRight: Bool = false

    init() {
        super.init(from: "vespa.scnassets/vespa_red.dae")
        //physicsBody = SCNPhysicsBody()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func animate() {
        
        if animating { return }
        animating = true
        
        let rotateOne = SCNAction.rotateBy(x: 0, y: CGFloat(Float.pi * 2), z: 0, duration: 5.0)
        let repeatForever = SCNAction.repeatForever(rotateOne)
        
        runAction(repeatForever)
    }
    
    func stopAnimating() {
        removeAllActions()
        animating = false
    }
    
    // ======== MOVEMENTS ======== //
    func moveForward(byDistance : CGFloat) {
        
        if movingForward {
            return
        }else{

            stopMoving()
            
            rotateInDirection(angle: CGFloat.pi)
            
            movingForward = true
            
            let movement =  SCNAction.moveBy(x: 0, y: 0, z: byDistance, duration: 5.0)
            let repeatForever = SCNAction.repeatForever(movement)
            
            runAction(repeatForever)
        }
    }
    
    func moveBack(byDistance : CGFloat) {
        
        if movingBack {
            return
        }else{

            stopMoving()
            
            rotateInDirection(angle: -CGFloat.pi)
            
            movingBack = true
            
            let movement =  SCNAction.moveBy(x: 0, y: 0, z: -byDistance, duration: 5.0)
            let repeatForever = SCNAction.repeatForever(movement)
            
            runAction(repeatForever)
        }
    }
    
    func moveLeft(byDistance : CGFloat) {
        
        if movingLeft {
            return
        }else{

            stopMoving()
            
            rotateInDirection(angle: -CGFloat.pi*1/2)
            
            movingLeft = true
            
            let movement =  SCNAction.moveBy(x: -byDistance, y: 0, z: 0, duration: 5.0)
            let repeatForever = SCNAction.repeatForever(movement)
            
            runAction(repeatForever)
        }
    }
    
    func moveRight(byDistance : CGFloat) {
        
        
        
        if movingRight {
            return
        }else{

            stopMoving()
            
            rotateInDirection(angle: -CGFloat.pi*3/2)
            
            movingRight = true
            
            let movement =  SCNAction.moveBy(x: byDistance, y: 0, z: 0, duration: 5.0)
            let repeatForever = SCNAction.repeatForever(movement)
            
            runAction(repeatForever)
        }
    }
    
    func rotateInDirection(angle : CGFloat){
        let rotateOne = SCNAction.rotateTo(x: 0, y: angle, z: 0 , duration: 0.5, usesShortestUnitArc: false)
                runAction(rotateOne)

    }
    
    func stopMoving() {
        
        if(movingForward || movingBack || movingRight || movingLeft){
            
            movingForward = false
            movingBack  = false
            movingLeft = false
            movingRight = false
            
            removeAllActions()
        }
    }
    
}

