//
//  HandSigns.swift
//  GestureRecognition
//
//  Created by Alessio Spina on 30/03/2019.
//  Copyright © 2019 Alessio Spina. All rights reserved.
//

import Foundation



enum HandSigns: String {
    case Left = "Left"
    case Right = "Right"
    case Close = "Close"
    case Open = "Open"
}
