//
//  SceneObject.swift
//  GestureRecognition
//
//  Created by Alessio Spina on 28/03/2019.
//  Copyright © 2019 Alessio Spina. All rights reserved.
//

import Foundation
import SceneKit

class SceneObject: SCNNode {
    
    init(from file: String) {
        super.init()
        
        let nodesInFile = SCNNode.allNodes(from: file)
        nodesInFile.forEach { (node) in
            self.addChildNode(node)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
