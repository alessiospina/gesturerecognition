//
//  MainScene.swift
//  GestureRecognition
//
//  Created by Alessio Spina on 28/03/2019.
//  Copyright © 2019 Alessio Spina. All rights reserved.
//

import Foundation
import ARKit



// custom scene class
struct MainScene {
    var scene: SCNScene?
    
    init() {
        // creation scene object
        self.scene = SCNScene()
        
        if let scene = self.scene {
            // set scene default value
            setDefaults(scene: scene)
        }
    }
    
    
    private func setDefaults(scene: SCNScene) {
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light?.type = SCNLight.LightType.ambient
        ambientLightNode.light?.color = UIColor(white: 0.6, alpha: 1.0)
        
        scene.rootNode.addChildNode(ambientLightNode)
        
        let directionalLight = SCNLight()
        directionalLight.type = .directional
        let directionalNode = SCNNode()
        directionalNode.eulerAngles = SCNVector3Make(GLKMathDegreesToRadians(-130), GLKMathDegreesToRadians(0), GLKMathDegreesToRadians(35))
        directionalNode.light = directionalLight
        
        scene.rootNode.addChildNode(directionalNode)
    }
    
    
//    timing function that we’ll pass to a scale-up animation. This will let us control the timing curve when the scaling reveal happens.
    func easeOutElastic(_ t: Float) -> Float {
        let p: Float = 0.3
        let result = pow(2.0, -10.0 * t) * sin((t - p / 4.0) * (2.0 * Float.pi) / p) + 1.0
        return result
    }
    
    
//    Add Vespa Object
    func addVespa(parent: SCNNode, position: SCNVector3) {
        
        guard let scene = self.scene else { return }
        
        let vespa = VespaObject()
        
        let prevScale = vespa.scale
        
        vespa.scale = SCNVector3(0.01, 0.01, 0.01)
        let scaleAction = SCNAction.scale(to: CGFloat(prevScale.x), duration: 1.5)
        scaleAction.timingMode = .linear
        
        scaleAction.timingFunction = { (p: Float) in
            return self.easeOutElastic(p)
        }
        
        vespa.name = "Vespa"
        vespa.position = scene.rootNode.convertPosition(position, to: parent)
        parent.addChildNode(vespa)
        vespa.runAction(scaleAction, forKey: "scaleAction")
    }

}


